package com.ksy.fitnessmanager.service;

import com.ksy.fitnessmanager.entity.PersonalTrainingCustomer;
import com.ksy.fitnessmanager.model.CustomerItem;
import com.ksy.fitnessmanager.model.CustomerRequest;
import com.ksy.fitnessmanager.repository.PersonalTrainingCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final PersonalTrainingCustomerRepository repository;

    public void setData(CustomerRequest request) {
        PersonalTrainingCustomer addData = new PersonalTrainingCustomer();

        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setHeight(request.getHeight());
        addData.setWeight(request.getWeight());
        addData.setFirstDate(LocalDateTime.now());

        repository.save(addData);
    }

    public List<CustomerItem> getCustomers() {
        List<PersonalTrainingCustomer> originData = repository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        for (PersonalTrainingCustomer item : originData) {
            CustomerItem addData = new CustomerItem();

            addData.setCustomerName(item.getCustomerName());
            addData.setCustomerPhone(item.getCustomerPhone());
            addData.setHeight(item.getHeight());
            addData.setWeight(item.getWeight());
            addData.setFirstDate(item.getFirstDate());
            addData.setLastDate(item.getLastDate());

            float heightDouble = (item.getHeight() / 100) * (item.getHeight() / 100);
            float bmi = item.getWeight() / heightDouble;

            addData.setBmi(bmi);

            String bmiResult = "";
            if (bmi <= 18.5) {
                bmiResult = "저체중";
            } else if (bmi <= 22.9) {
                bmiResult = "정상";
            } else if (bmi <= 24.9) {
                bmiResult = "과체중";
            } else {
                bmiResult = "비만";
            }

            addData.setBmiResult(bmiResult);

            result.add(addData);
        }

        return result;
    }
}
