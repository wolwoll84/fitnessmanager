package com.ksy.fitnessmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerItem {
    private String customerName;
    private String customerPhone;
    private Float height;
    private Float weight;
    private LocalDateTime firstDate;
    private LocalDateTime lastDate;
    private Float bmi;
    private String bmiResult;
}
