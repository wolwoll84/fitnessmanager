package com.ksy.fitnessmanager.controller;

import com.ksy.fitnessmanager.model.CustomerItem;
import com.ksy.fitnessmanager.model.CustomerRequest;
import com.ksy.fitnessmanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerService service;

    @PostMapping("/data")
    public String setData(@RequestBody @Valid CustomerRequest request) {
        service.setData(request);
        return "등록되었습니다.";
    }

    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        return service.getCustomers();
    }
}
