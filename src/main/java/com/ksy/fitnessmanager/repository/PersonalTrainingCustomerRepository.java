package com.ksy.fitnessmanager.repository;

import com.ksy.fitnessmanager.entity.PersonalTrainingCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonalTrainingCustomerRepository extends JpaRepository<PersonalTrainingCustomer, Long> {
}
