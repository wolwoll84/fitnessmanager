package com.ksy.fitnessmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @NotNull
    @Length(min = 1, max = 20)
    private String customerPhone;

    @NotNull
    private Float height;

    @NotNull
    private Float weight;
}
